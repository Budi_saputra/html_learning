# HTML Learning 

## What is HTML ?

[Wikipedia Bahasa Indonesia](https://id.wikipedia.org/wiki/HTML) <br>
[Wikipedia.org](https://en.wikipedia.org/wiki/HTML)

    Konsep analogi nya seperti ini , Jika kita membuat rumah Hal pertama yang akan di buat pasti Kerangka atau Pondasi rumah dahulu , belum indah di lihat tapi sudah berbentuk rumah karena sudah ada pondasi rumah  , begitu juga dengan HTML .

## HTML Intro

    <!DOCTYPE html> //* Mendeklarasikan Versi HTML5
    <html> //* Element root HTML
    <head> //* Element contains seperti title,meta,css,dll
        <title>Halaman HTML Pertama</title> //* Judul 
    </head>
    <body> 

        <h1>Selamat Datang</h1>
        <p>Ini adalah halaman HTML pertama saya, Maybe....</p>
        
    </body>
    </html> 
